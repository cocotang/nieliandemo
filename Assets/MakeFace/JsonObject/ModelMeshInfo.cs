﻿using System;
using System.Collections.Generic;

public class JsonVector3 {
    public double x { get; set; }
    public double y { get; set; }
    public double z { get; set; }
}

public class JsonVector2
{
    public double x { get; set; }
    public double y { get; set; }
}


public class ModelMeshInfo
{
    public List<JsonVector3> vertexPos { get; set; }
    public int[] triangles { get; set; }
    public List<JsonVector3> normals { get; set; }
    public List<JsonVector2> uv { get; set; }
}
