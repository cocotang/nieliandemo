﻿using System;
using System.Collections.Generic;

public class BoneInfo
{
    public int boneIndex { get; set; }
    public List<int> vertexIndexs { get; set; }
    public List<double> vertexWeights { get; set; }
}

public class ModelBonesInfo
{
    public List<BoneInfo> boneInfo { get; set; }
}

