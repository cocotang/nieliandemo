﻿using System;
using System.Collections.Generic;

public class BonePos
{
    public int boneIndex { get; set; }
    public JsonVector3 bonePos { get; set; }
    public List<BoneVertexOffset> vertexOffsets { get; set; }
}

public class BoneVertexOffset
{
    public int vertexIndex { get; set; }
    public JsonVector3 org { get; set; }
    public JsonVector3 dtMin { get; set; }
    public JsonVector3 dtMax { get; set; }
}

/// <summary>
/// 缓存上bone偏移影响的所有顶点位置
/// </summary>
public class ModelBonesOffset
{
    public List<BonePos> bones { get; set; }
}

