﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace TwlMakeFace
{
    public class CombineTexture
    {
        
        public static Texture2D MergeTexture(Texture2D t1,Texture2D t2,int offsetX, int offsetY) {
            Texture2D faceTex;
            faceTex = new Texture2D(t1.width,t1.height, TextureFormat.ARGB32,false);
            faceTex.SetPixels(t1.GetPixels());
            for (int x = 0; x < t2.width; x++)
            {
                for (int y = 0; y < t2.height; y++)
                {
                    var PixelColorFore = t2.GetPixel(x, y) * t2.GetPixel(x, y).a;
                    var newY = t1.height - offsetY - t2.height + y;
                    var PixelColorBack = t1.GetPixel(x + offsetX, newY) * t1.GetPixel(x + offsetX, newY).a;
                    faceTex.SetPixel(x + offsetX, newY, PixelColorFore + PixelColorBack);
                }
            }
            faceTex.Apply();
            return faceTex;
        }


        public static Texture BlitMerge(Texture2D t1, Texture2D t2) {
            CommandBuffer commandBuffer = new CommandBuffer();
            RenderTexture rt = new RenderTexture(1024,1024,24, RenderTextureFormat.ARGB32);
            commandBuffer.Blit(t1,rt);
            commandBuffer.Blit(t2, rt);
            return rt as Texture;
        }

    }
}
