﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class UISlider : MonoBehaviour
{
    private Text nameText;
    private Slider slider;

    private BoneInfo info;
    private Action<int, float> callBack;
    private bool init = false;
    private string sName="";


    private void Start()
    {
        slider = this.GetComponent<Slider>();
        nameText = this.transform.Find("nameText").GetComponent<Text>();
        init = true;
        refreshUI();
    }

    public void setBoneInfo(BoneInfo info, string name ,Action<int, float> callBack)
    {
        this.info = info;
        this.callBack = callBack;
        this.sName = name;
        refreshUI();
    }

    private void refreshUI()
    {
        if (!init || info == null) return;
        nameText.text = this.sName;
        slider.value = 0.5f;
        slider.onValueChanged.AddListener(onValueChange);
    }

    private void onValueChange(float val)
    {
        if (callBack != null) {
            callBack(info.boneIndex,val);
        }
    }



}
